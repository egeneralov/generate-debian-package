# syntax=docker/dockerfile:1.4
FROM golang:1.20.2

ARG REPO_NAME=gitlab.com/egeneralov/gitlab-wait-for
WORKDIR /go/src/${REPO_NAME}
ADD go.mod go.sum /go/src/${REPO_NAME}/
ARG GOPROXY="https://proxy.golang.org,direct"
ARG GOPRIVATE=""
RUN go mod download -x
ADD . .
ARG COMMAND=gitlab-wait-for
ARG CGO_ENABLED=0
RUN --mount=type=cache,target=/root/.cache/go-build go build -v -ldflags "-linkmode auto -w" -v -o /go/bin/${COMMAND} ${REPO_NAME}/cmd/${COMMAND}

FROM debian:11
ARG COMMAND=gitlab-wait-for
COPY --from=0 /go/bin/${COMMAND} /bin/
CMD ["/bin/${COMMAND}"]
