package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"

	"github.com/Masterminds/semver"
	"github.com/cristalhq/aconfig"
	"github.com/cristalhq/aconfig/aconfigdotenv"
	"github.com/cristalhq/aconfig/aconfighcl"
	"github.com/cristalhq/aconfig/aconfigtoml"
	"github.com/cristalhq/aconfig/aconfigyaml"
)

type Control struct {
	Package      string   `env:"CI_PROJECT_NAME"`
	Section      string   `env:"SECTION" default:"misc"`
	Priority     string   `env:"PRIORITY" default:"optional"`
	Architecture string   `env:"GOARCH"`
	Maintainer   string   `env:"CI_COMMIT_AUTHOR"`
	Depends      []string `env:"DEPENDS"`
	Description  string   `env:"DESCRIPTION"`
	Directory    string   `env:"DIRECTORY"`
	Size         int64
	Version      *semver.Version
	Output       string
	PkgName      string `env:"PKG_NAME"`
	PkgPath      string `env:"PKG_PATH"`
	BinarySrc    string `env:"BINARY_SRC" flag:"binary-src"`
	BinaryName   string `env:"BINARY_NAME" flag:"binary-name"`
	Auto         bool
}

func (c *Control) Load() error {
	acfg := aconfig.Config{
		EnvPrefix:        "",
		FlagDelimiter:    "",
		AllFieldRequired: false,
		AllowDuplicates:  false,
		DontGenerateTags: false,
		MergeFiles:       true,
		FileFlag:         "config",
		Files: []string{
			".gdp.hcl",
			".gdp.yaml",
			".gdp.yml",
			".gdp.toml",
			".gdp.env",
		},
		FileDecoders: map[string]aconfig.FileDecoder{
			".hcl":  aconfighcl.New(),
			".yml":  aconfigyaml.New(),
			".yaml": aconfigyaml.New(),
			".toml": aconfigtoml.New(),
			".env":  aconfigdotenv.New(),
		},
	}
	return aconfig.LoaderFor(c, acfg).Load()
}

func NewControl() (Control, error) {
	c := Control{}
	if err := c.Load(); err != nil {
		return Control{}, err
	}
	if c.Description == "" {
		c.Description = fmt.Sprintf(
			"%s %s %s\n %s",
			os.Getenv("CI_PROJECT_TITLE"),
			os.Getenv("CI_COMMIT_REF_NAME"),
			os.Getenv("CI_PIPELINE_CREATED_AT"),
			os.Getenv("CI_PIPELINE_URL"),
		)
	}
	return c, nil
}

func (c *Control) String() string {
	s := ""
	s += fmt.Sprintf("Package: %s\n", c.Package)
	s += fmt.Sprintf("Version: %s\n", c.Version)
	s += fmt.Sprintf("Section: %s\n", c.Section)
	s += fmt.Sprintf("Priority: %s\n", c.Priority)
	s += fmt.Sprintf("Architecture: %s\n", c.Architecture)
	s += fmt.Sprintf("Size: %d\n", c.Size)
	s += fmt.Sprintf("Maintainer: %s\n", c.Maintainer)
	s += fmt.Sprintf("Depends: %s\n", strings.Join(c.Depends, ", "))
	s += fmt.Sprintf("Description: %s\n", c.Description)
	return s
}

func (c *Control) Validate() error {
	if c.Auto {
		return nil
	}
	if c.Output == "" {
		return fmt.Errorf("output file must be specified")
	}
	if c.Directory == "" {
		return fmt.Errorf("directory for size calculation must be specified")
	}
	return nil
}

func (c *Control) VersionFromEnvironment() error {
	var (
		rawVersion string
		err        error
	)
	if candidate := os.Getenv("CI_COMMIT_TAG"); candidate != "" {
		rawVersion = candidate
	} else {
		rawVersion = "0.0.0-" + os.Getenv("CI_COMMIT_SHA")
	}
	c.Version, err = semver.NewVersion(rawVersion)
	if err != nil {
		if errors.Is(err, semver.ErrInvalidSemVer) {
			return fmt.Errorf("failed to parse version from CI_COMMIT_TAG/CI_COMMIT_SHA: %w", err)
		}
		return err
	}
	//- PKG_NAME="${CI_PROJECT_NAME}_${CI_COMMIT_TAG:-0.0.0-${CI_COMMIT_SHA}}-${CI_PIPELINE_IID}"
	if c.PkgName == "" {
		c.PkgName = fmt.Sprintf("%s_%s", c.Package, c.Version)
		if iid := os.Getenv("CI_PIPELINE_IID"); iid != "" {
			c.PkgName += "-" + iid
		}
	}
	//- PKG_PATH="${RUNNER_TEMP_PROJECT_DIR}/${PKG_NAME}"
	if c.PkgPath == "" {
		if tmpdir := os.Getenv("RUNNER_TEMP_PROJECT_DIR"); tmpdir == "" {
			tmpdir = "/tmp"
			c.PkgPath = path.Join(tmpdir, c.PkgName)
		}
	}
	return nil
}

func (c *Control) WriteFile(mode os.FileMode) error {
	if err := os.WriteFile(c.Output, []byte(c.String()), mode); err != nil {
		return fmt.Errorf("failed to write file '%s': %w", c.Output, err)
	}
	return nil
}

func (c *Control) dirSize(path string) (int64, error) {
	var size int64
	err := filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}

func (c *Control) DirSize() (err error) {
	c.Size, err = DirSize(c.Directory)
	if err != nil {
		return fmt.Errorf("failed to calculate directory size: %v\n", err)
	}
	return nil
}

func (c *Control) AIO() error {
	if c.BinarySrc == "" || c.BinaryName == "" {
		return fmt.Errorf("binary src/name can't be empty")
	}
	//mkdir
	for _, sub := range []string{"/usr/local/bin", "/DEBIAN"} {
		dir := path.Join(c.PkgPath, sub)
		if err := os.MkdirAll(dir, 0755); err != nil {
			return fmt.Errorf("failed to create directory '%s': %w", dir, err)
		}
	}
	//cp binary
	dst := path.Join(c.PkgPath, "/usr/local/bin", c.BinaryName)
	if err := cp(c.BinarySrc, dst, 10000); err != nil {
		return fmt.Errorf("failed to copy '%s' to '%s': %w", c.BinarySrc, dst, err)
	}
	c.Output = path.Join(c.PkgPath, "DEBIAN/control")
	c.Directory = c.PkgPath
	//DEBIAN/control
	if err := c.WriteFile(0664); err != nil {
		return fmt.Errorf("failed to write file: %w", err)
	}
	if err := c.DirSize(); err != nil {
		return fmt.Errorf("failed to calc directory size: %w", err)
	}
	for _, rawCommand := range []string{
		fmt.Sprintf("dpkg --build %s", c.PkgPath),
		fmt.Sprintf("dpkg -I %s.deb", c.PkgPath),
		fmt.Sprintf("mv %s.deb %s/", c.PkgPath, os.Getenv("CI_PROJECT_DIR")),
	} {
		split := strings.Split(rawCommand, " ")
		buf := bytes.NewBuffer(nil)
		cmd := exec.Command(split[0], split[1:]...)
		cmd.Stdout = buf
		if err := cmd.Run(); err != nil {
			return fmt.Errorf("failed to run cmd %s: %w", rawCommand, err)
		}
	}
	return nil
}

func cp(src, dst string, bufferSize int64) error {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return err
	}
	defer func(source *os.File) {
		err := source.Close()
		if err != nil {
			// TODO log me
			return
		}
	}(source)

	_, err = os.Stat(dst)
	if err == nil {
		return fmt.Errorf("file %s already exists", dst)
	}

	destination, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer func(destination *os.File) {
		err := destination.Close()
		if err != nil {
			// TODO: log me
			return
		}
	}(destination)

	if err != nil {
		panic(err)
	}

	buf := make([]byte, bufferSize)
	for {
		n, err := source.Read(buf)
		if err != nil && err != io.EOF {
			return err
		}
		if n == 0 {
			break
		}

		if _, err := destination.Write(buf[:n]); err != nil {
			return err
		}
	}
	return err
}
