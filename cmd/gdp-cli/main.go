package main

import (
	"errors"
	"flag"
	"os"
	"path/filepath"
)

func DirSize(path string) (int64, error) {
	var size int64
	err := filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}

func main() {
	control, err := NewControl()
	if err != nil {
		if errors.Is(err, flag.ErrHelp) {
			os.Exit(0)
		}
		panic(err)
	}
	if err = control.Validate(); err != nil {
		panic(err)
	}
	if err = control.VersionFromEnvironment(); err != nil {
		panic(err)
	}
	if control.Auto {
		if err = control.AIO(); err != nil {
			panic(err)
		}
	} else {
		if err = control.DirSize(); err != nil {
			panic(err)
		}
		if err = control.WriteFile(0664); err != nil {
			panic(err)
		}
	}
}
