#!/usr/bin/env bash
set -e

export \
  DEPENDS="bash, cli" \
  GOARCH="arm64" \
  CI_PROJECT_NAME="gitlab-wait-for" \
  CI_PROJECT_TITLE="gitlab-wait-for" \
  CI_COMMIT_REF_NAME="master" \
  CI_PIPELINE_CREATED_AT="2023-03-07T16:01:10Z" \
  CI_PIPELINE_URL="https://gitlab.com/egeneralov/gitlab-wait-for/-/pipelines/798843734" \
  DESCRIPTION="" \
  CI_COMMIT_SHA="901af696b7bed98f778eb0aac5d606f7e426d38c" \
  CI_COMMIT_AUTHOR="Eduard Generalov <eduard@generalov.net>" \
#  CI_COMMIT_TAG="1.1.1"

rm -f /tmp/control
go run cmd/gdp-cli/*.go -directory . -output /tmp/control
cat /tmp/control
ls -lha /tmp/control
stat /tmp/control
