module gitlab.com/egeneralov/generate-debian-package

go 1.19

require (
	github.com/Masterminds/semver v1.5.0
	github.com/cristalhq/aconfig v0.18.3
	github.com/cristalhq/aconfig/aconfigdotenv v0.17.1
	github.com/cristalhq/aconfig/aconfighcl v0.17.1
	github.com/cristalhq/aconfig/aconfigtoml v0.17.1
	github.com/cristalhq/aconfig/aconfigyaml v0.17.1
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
